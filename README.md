# Node-Cache-Layer

This node module is a framework for cache implementations used by any node api.

It aims to be a feature-driven system that will support:

* cache refresh schedulers
* key based refresh/invalidation
* multiple cache engines
* multiple coordination services

The module doesn't contain any business logic and delegates that to the running application.

## Installation

```
$ npm install git+https://git@bitbucket.org:mindera/node-cache-layer.git#v1.0.0
```

The module follows [semver](http://semver.org/) so make sure to pin your dependency version accordingly.

## Usage

The module exposes an API to specify its configuration as well as to trigger desired behaviours and actions.

Common patterns across most of this module methods:

* `keyList` is a map of keys and values (or http request definitions) used to refresh key values in the cache engine

### Initialization

The init method of the module expects a configuration object with the following options:

TODO

Example:

```
var cacheLayer = require('node-cache-layer');

cacheLayer.init({
    cache: {
        client: 'redis',
        redis: {
            post: 'localhost',
            port: 6379,
            enableOfflineQueue: true,
            retryMaxDelay: 10000
        }
    },
    coordinator: {
        client: 'etcd',
        etcd: {
            host: 'localhost',
            port: 4001,
            leaderKey: '/master',
            keyExpireWindow: 5
        }
    },
    scheduler: {
        requireLeadership: true,
        firstRunOnly: false,
        maxKeysPerRun: 5,
        delayPerRun: 15000
    },
    http: {
        requestTimeout: 5000,
        responseTimeout: 10000
    },
    manager: {}
});
```

This configuration is defined once and used by all methods of the module.

### Key Based Refresh/Invalidation

All key based refresh/invalidation methods return a `callback(err)` following the error first callback pattern.

The following key based refresh methods are available:

#### Refresh Keys - refreshKeys(keyList, callback)

Used to refresh keys with the response of the correspondent http request to an origin.

```
/*
Sample data:

{
    "httpDefaults": {
        "hostname": "www.mocky.io",
        "port": 80,
        "method": "GET"
    },
    "keys": [
        {
            "key": "namespace_x_y_foo",
            "httpOptions": {
                "path": "/v2/55b0ef6f2267c91a084e9a93"
            }
        },
        {
            "key": "namespace_x_y_bar",
            "httpOptions": {
                "path": "/v2/55b0ef6f2267c91a084e9a93"
            }
        }
    ]
}
*/

// data is a JSON.parse of the json sample above
cacheLayer.refreshKeys(data, function handleRefres(err) {
    if (err) {
        console.error(err);
    } else {
        console.log('all keys updated');
    }
});

```

#### Refresh and Return Key - refreshAndReturnKey(key, callback)

Used to refresh and return a single key with the response of the correspondent http request to an origin.

```
/*
Sample data:
{
    "key": "namespace_x_y_foo",
    "httpOptions": {
         "hostname": "www.mocky.io",
         "port": 80,
         "method": "GET",
         "path": "/v2/55b0ef6f2267c91a084e9a93"
    }
}
*/

// data is a JSON.parse of the json sample above
cacheLayer.refreshAndReturnKey(data, function handleRefreshAndReturnKey(err, response) {
    if (err) {
        console.error(err);
        if (response) {
            console.log(response);
        }
    } else {
        console.log(response);
    }
});
```

#### Refresh with Value - refreshKeysWithValue(keyList, callback)

Used to refresh keys with a specified value.

```
/*
Sample data:
{
    "keys": [
        {
            "key": "namespace_x_y_foo",
            "value": "XYFOO"
        },
        {
            "key": "namespace_x_y_bar",
            "value": "XYBAR"
        }
    ]
}
*/

// data is a JSON.parse of the json sample above
cacheLayer.refreshKeysWithValue(data, function handleRefreshWithValue(err) {
    if (err) {
        console.error(err);
    } else {
        console.log('all keys updated');
    }
});
```

#### Invalidation by Key - invalidateKeys(keys, callback)

TODO

#### Invalidation by Key Pattern - invalidateKeysByPattern(pattern, callback)

TODO

These methods can, for instance, be used within controllers of a web api to handle key refresh/invalidation on demand.

### Cache Refresh Scheduler

#### Refresh Keys on a Scheduler - refreshScheduler([arg1, arg2, ...], mapFunc)

Used to create a refresh scheduler to update keys with the response of the correspondent http request to an origin.

It receives an array of arguments to me passed into a mapping function which return a `callback(keyList)` which in turn will trigger a new scheduler.

The generated `keyList` must follow the same data sample as the `refreshKeys` example above.

```
function mapFunc(arg1, arg2, callback) {

    // do something with arg1 and arg2 and convert it to keyList

    return callback(keyList);
}

cacheLayer.refreshScheduler([arg1, arg2], mapFunc);
```

The scheduler supports running in a "leader only mode" which guarantees that only a single instance is refreshing thecache at any point in time.

For more details on this see the *Coordination Service* entry below.

## HTTP Client

The module has a built-in http client which is really just a wrapper of the standard [http](https://nodejs.org/api/http.html) and [https](https://nodejs.org/api/http.html) modules.

All methods which require http interaction to fetch key values from origins use this client to perform the actual http requests.

General configuration for this client is done in the initialization step such as:

```
    http: {
        requestTimeout: 5000,
        responseTimeout: 10000
    }
```

You can configure further configure the client by passing the `httpDefaults` and the `httpOptions` objects in the `keyList` map on refresh methods.

Both objects are merged in runtime and `httpDefaults` values are overwritten by the `httpOptions` values on a per request basis.

Example:

```
/*
Sample data:

{
    "httpDefaults": {
        "hostname": "www.mocky.io",
        "port": 80,
        "method": "GET"
    },
    "keys": [
        {
            "key": "namespace_x_y_foo",
            "httpOptions": {
                "path": "/v2/55b0ef6f2267c91a084e9a93"
            }
        },
        {
            "key": "namespace_x_y_bar",
            "httpOptions": {
                "path": "/v2/55b0ef6f2267c91a084e9a93"
            }
        }
    ]
}
*/
```

Available options:

TODO

## Cache Engine

The module is built to support multiple cache engines - the choice of which to use is done in the initialization step.

Currently the following engines are supported:

### Redis

To use the Redis engine set `options.cache.client='redis'` and pass it a Redis instance/cluster details such as:

```
    cache: {
        client: 'redis',
        redis: {
            post: 'localhost',
            port: 6379,
            enableOfflineQueue: true,
            retryMaxDelay: 10000
        }
    }
```

Available options:

TODO

## Coordination Service

The module is built to support multiple coordination services in order to guarantee things like leader election for
"leader only mode" schedulers - the choice of which to use is done in the initialization step.

Currently the following services are supported:

### Etcd

To use the Etcd service set `options.coordinator.client='etcd'` and pass it an Etcd instance/cluster details such as:

```
    coordinator: {
        client: 'etcd',
        etcd: {
            host: 'localhost',
            port: 4001,
            leaderKey: '/master',
            keyExpireWindow: 5
        }
    }
```

Available options:

TODO

## Development

### Setup

TODO

### Contributing

Development follows the Pull Request model which means that if you want to contribute you should:

* Clone/Fork the repo
* Create a feature/defect branch
* Pull Request back to the Origin/Upstream master branch

### Commit Messages Format

To ensure we have an automated consistent change log please use the following format for commit messages:

```
fix(PROJ-123): add http timeout config
```

The tags must be one of the following:

* feat: A new feature
* fix: A bug fix
* docs: Documentation only changes
* style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* refactor: A code change that neither fixes a bug or adds a feature
* perf: A code change that improves performance
* test: Adding missing tests
* chore: Changes to the build process or auxiliary tools and libraries such as documentation generation

### Feature Backlog

* Module should be an event emitter so that events such as errors and exceptions can be safely handled

## Tests

TODO
