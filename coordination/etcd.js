'use strict';

var Etcd = require('node-etcd');
var etcdLeader = require('etcd-leader');
var os = require('os');

module.exports = {
    /**
     * Initializes the etcd coordinator client
     * @param {object} options - etcd coordinator client options
     * @returns {EtcdLeader}
     */
    init: function (options) {
        var id = os.hostname() + '-' + process.pid;

        if (options.etcd.host) {
            if (typeof options.etcd.host !== 'string') {
                throw new TypeError('etcd.host is not a string: ' + typeof options.etcd.host);
            }
        }

        if (options.etcd.port) {
            if (typeof options.etcd.port !== 'number') {
                throw new TypeError('etcd.port is not a number: ' + typeof options.etcd.port);
            }
        }

        if (options.etcd.leaderKey) {
            if (typeof options.etcd.leaderKey !== 'string') {
                throw new TypeError('etcd.leaderKey is not a string: ' + typeof options.etcd.leaderKey);
            }
        }

        if (options.etcd.keyExpireWindow) {
            if (typeof options.etcd.keyExpireWindow !== 'number') {
                throw new TypeError('etcd.keyExpireWindow is not a number: ' + typeof options.etcd.keyExpireWindow);
            }
        }

        // TODO: add cluster support
        // var etcdClusterSetup = options.etcdClusterSetup || false;

        var etcdHost = options.etcd.host || 'localhost';
        var etcdPort = options.etcd.port || 4001;
        var etcdLeaderKey = options.etcd.leaderKey || '/master';
        var etcdKeyExpireWindow = options.etcd.keyExpireWindow || 5;

        this.clientElection = etcdLeader(new Etcd(etcdHost, etcdPort), etcdLeaderKey, id, etcdKeyExpireWindow);

        return this;
    },

    startElection: function () {
        this.clientElection.start();
    },

    isRunning: function () {
        return this.clientElection.isRunning();
    },

    isLeader: function () {
        return this.clientElection.isLeader();
    }
};
