'use strict';

module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        nodeunit: {
            all: ['test/*_test.js'],
            options: {
                reporter: 'default'
            }
        },
        eslint: {
            options: {
                config: '.eslintrc'
            },
            gruntfile: {
                src: ['Gruntfile.js']
            },
            js: {
                src: [
                    '*.js',
                    'lib/**/*.js',
                    'cache/**/*.js',
                    'coordination/**/*.js'
                ]
            },
            test: {
                src: ['test/**/*.js']
            }
        },
        watch: {
            gruntfile: {
                files: '<%= eslint.gruntfile.src %>',
                tasks: ['eslint:gruntfile']
            },
            js: {
                files: '<%= eslint.js.src %>',
                tasks: ['eslint:js', 'nodeunit']
            },
            test: {
                files: '<%= eslint.test.src %>',
                tasks: ['eslint:test', 'nodeunit']
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Bump version v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: '%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                regExp: false
            }
        },
        versioncheck: {
            options: {
                hideUpToDate: true
            }
        }
    });

    grunt.registerTask('dev', [
        'watch'
    ]);

    grunt.registerTask('test', [
        'eslint',
        'nodeunit'
    ]);

    grunt.registerTask('ci', [
        'test'
    ]);

    grunt.registerTask('release:major', [
        'bump:major'
    ]);

    grunt.registerTask('release:minor', [
        'bump:minor'
    ]);

    grunt.registerTask('release:patch', [
        'bump:patch'
    ]);
};
