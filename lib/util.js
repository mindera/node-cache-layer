'use strict';

module.exports = {
    /**
     * Requires the correct coordinator service library
     * @param client
     * @returns {*}
     */
    requireCoordinatorClient: function (client) {
        switch (client) {
            case 'etcd':
                return require('./../coordination/etcd');
            case 'undefined':
                throw new Error('Coordinator client must be defined');
            default:
                throw new Error('Coordinator client %s not supported', client);
        }
    },

    /**
     * Requires the correct cache service library
     * @param client
     * @returns {*}
     */
    requireCacheClient: function (client) {
        switch (client) {
            case 'redis':
            case 'fakeredis':
                return require('./../cache/redis');
            case 'undefined':
                throw new Error('Cache client must be defined');
            default:
                throw new Error('Cache client %s not supported', client);
        }
    },

    sanitizeOptions: function (options) {
        var objects = [
            'manager',
            'scheduler',
            'http',
            'cache',
            'coordinator'
        ];

        objects.forEach(function (obj) {
            if (!options[obj]) {
                options[obj] = {};
            }
        });

        return options;
    }
};
