'use strict';

/**
 * Traverses an array of keys and refreshes its values based on correspondent http requests
 * @param {object} httpClient - http client instance
 * @param {object} cacheClient - cache client instance
 */
function refreshKeys(httpClient, cacheClient) {
    var currPos = this.nextKey;
    var nextPos = currPos + this.options.maxKeysPerRun;
    var nextArray = this.allKeys.slice(currPos, nextPos);

    nextArray.forEach(function iterateKeys(keyObj) {
        var key = keyObj.key;
        var httpOptions = {};

        // I really don't like the way this is being done...

        for (var defaultProperty in this.httpDefaults) {
            if (this.httpDefaults.hasOwnProperty(defaultProperty)) {
                httpOptions[defaultProperty] = this.httpDefaults[defaultProperty];
            }
        }

        for (var property in keyObj.httpOptions) {
            if (keyObj.httpOptions.hasOwnProperty(property)) {
                if (!httpOptions.hasOwnProperty(property)) {
                    httpOptions[property] = keyObj.httpOptions[property];
                }
            }
        }

        httpClient.request(httpOptions, function handleRequest(err, res) {
            if (err) {
                // TODO: handle error properly
            } else {
                var data = (typeof res === 'object') ? JSON.stringify(res) : res;

                cacheClient.set(key, data, function handleSet(err) {
                    if (err) {
                        // TODO: handle errors properly
                    }
                });
            }
        });
    }, this);

    if (this.allKeys[nextPos]) {
        if (nextPos === this.allKeys.length) {
            this.nextKey = nextPos - 1;
        } else {
            this.nextKey = nextPos;
        }
    } else {
        this.nextKey = 0;

        if (this.options.firstRunOnly) {
            clearInterval(this.interval);
        }
    }
}

module.exports = {
    // TODO: convert scheduler into an event emitter

    /**
     * Initializes the scheduler module
     * @param {object} options - scheduler module options
     * @returns {exports} - scheduler module instance
     */
    init: function (options) {
        // TODO: validate options

        this.options = {
            requireLeadership: options.requireLeadership || false,
            firstRunOnly: options.firstRunOnly || false,
            maxKeysPerRun: options.maxKeysPerRun || 5,
            delayPerRun: options.delayPerRun || 15000
        };

        return this;
    },

    /**
     * Schedules key refreshes in a cache engine based on a map of keys and correspondent http requests
     * It only performs this.options.maxKeysPerRun keys per run and delays each run by this.options.delayPerRun
     * @param {object} httpClient - http client instance
     * @param {object} cacheClient - cache client instance
     * @param {object} coordinatorClient - coordinator client instance
     * @param {object} keyList - object mapping cache keys and http request options to refresh
     */
    refreshKeys: function (httpClient, cacheClient, coordinatorClient, keyList) {
        // TODO: validate input

        if (keyList.httpDefaults) {
            this.httpDefaults = keyList.httpDefaults;
        }

        this.nextKey = 0;
        this.allKeys = keyList.keys;

        var self = this;
        var startRefreshInterval = function () {
            return setInterval(function () {
                refreshKeys.apply(self, [httpClient, cacheClient]);
            }, self.options.delayPerRun);
        };

        if (this.options.requireLeadership) {
            if (coordinatorClient.isRunning() && coordinatorClient.isLeader()) {
                this.interval = startRefreshInterval();
            }

            coordinatorClient.clientElection.on('elected', function () {
                self.interval = startRefreshInterval();
            });

            coordinatorClient.clientElection.on('unelected', function () {
                clearInterval(self.interval);
            });

            coordinatorClient.clientElection.on('error', function (err) {
                // TODO: handle error properly
            });
        } else {
            this.interval = startRefreshInterval();
        }

    }
};
