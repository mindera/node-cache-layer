'use strict';

var http = require('http');
var https = require('https');

module.exports = {

    /**
     * Initializes the http client module
     * @param {object} options - http client options
     * @returns {exports} - httpClient instance
     */
    init: function (options) {
        if (options.requestTimeout) {
            if (typeof options.requestTimeout !== 'number') {
                throw new TypeError('requestTimeout is not a number: ' + typeof options.requestTimeout);
            }
        }

        if (options.responseTimeout) {
            if (typeof options.responseTimeout !== 'number') {
                throw new TypeError('responseTimeout is not a number: ' + typeof options.requestTimeout);
            }
        }

        this.httpOptions = {
            requestTimeout: options.requestTimeout || 2000,
            responseTimeout: options.responseTimeout || 5000
        };

        return this;
    },

    /**
     * Execute http request
     * @param {object} options - http request options (same as the http standard lib)
     * @param {function} callback
     */
    request: function (options, callback) {
        // TODO: validate options

        var httpClient = (options.port === 443) ? https : http;

        var request = httpClient.request(options, function handlePost(response) {
            var resData = '';

            response.on('data', function (chunk) {
                resData += chunk;
            });

            response.on('end', function () {
                if (response.statusCode === 200) {
                    var resContent;
                    try {
                        resContent = JSON.parse(resData);
                    } catch (ex) {
                        resContent = resData;
                    }

                    return callback(null, resContent);
                } else {
                    return callback(new Error('Unsuccessful response: %s %s',
                        response.statusCode, response.statusMessage));
                }
            });

            //response.setTimeout(this.httpOptions.responseTimeout);
        });

        request.on('error', function(err) {
            return callback(err);
        });

        //request.setTimeout(this.httpOptions.requestTimeout);

        if (options.method === 'POST') {
            request.write(options.postData);
        }

        request.end();
    }
};

