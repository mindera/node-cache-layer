'use strict';

var async = require('async');

module.exports = {
    /**
     * Initializes the manager module
     * @param {object} options - manager module options
     * @returns {exports} - manager module instance
     */
    init: function (options) {
        // TODO: validate options

        return this;
    },

    /**
     * Refresh keys in a cache engine based on a map of keys and correspondent http requests
     * @param {object} httpClient - http client instance
     * @param {object} cacheClient - cache client instance
     * @param {object} keyList - object mapping cache keys and http request options to refresh
     * @param callback
     */
    refreshKeys: function(httpClient, cacheClient, keyList, callback) {
        // TODO: validate inputs

        var httpDefaults;

        if (keyList.httpDefaults) {
            httpDefaults = keyList.httpDefaults;
        }

        async.each(keyList.keys,
            function (keyObj, itemCallback) {
                var httpOptions = httpDefaults;
                var key = keyObj.key;

                for (var property in keyObj.httpOptions) {
                    if (keyObj.httpOptions.hasOwnProperty(property)) {
                        if (!httpOptions.hasOwnProperty(property)) {
                            httpOptions[property] = keyObj.httpOptions[property];
                        }
                    }
                }

                httpClient.request(httpOptions, function handleRequest(err, res) {
                    if (err) {
                        itemCallback(err);
                    } else {
                        var data = (typeof res === 'object') ? JSON.stringify(res) : res;

                        cacheClient.set(key, data, function handleSet(err) {
                            if (err) {
                                itemCallback(err);
                            } else {
                                itemCallback();
                            }
                        });
                    }
                });
            },
            function (err) {
                if (err) {
                    return callback(err);
                } else {
                    return callback(null);
                }
            }
        );
    },

    /**
     * Refresh keys in a cache engine based on a map of keys and correspondent values
     * @param {object} cacheClient - cache client instance
     * @param {object} keyList - object mapping cache keys and values to refresh
     * @param callback
     */
    refreshKeysWithValue: function (cacheClient, keyList, callback) {
        // TODO: validate inputs

        async.each(keyList.keys,
            function (keyObj, itemCallback) {
                var value = keyObj.value;
                var key = keyObj.key;

                cacheClient.set(key, value, function handleSet(err) {
                    if (err) {
                        itemCallback(err);
                    } else {
                        itemCallback();
                    }
                });
            },
            function (err) {
                if (err) {
                    return callback(err);
                } else {
                    return callback(null);
                }
            }
        );
    },

    /**
     * Refresh a single key in a cache engine based on a map of key and correspondent http request and return the response
     * @param {object} httpClient - http client instance
     * @param {object} cacheClient - cache client instance
     * @param {object} keyList - object mapping cache keys and values to refresh
     * @param {object} key - object mapping cache key and http request options to refresh
     * @param callback
     */
    refreshAndReturnKey: function(httpClient, cacheClient, key, callback) {
        // TODO: validate inputs

        httpClient.request(key.httpOptions, function handleRequest(err, res) {
            if (err) {
                return callback(err);
            } else {
                var data = (typeof res === 'object') ? JSON.stringify(res) : res;

                cacheClient.set(key.key, data, function handleSet(err) {
                    if (err) {
                        return callback(err, res);
                    } else {
                        return callback(null, res);
                    }
                });
            }
        });
    },

    /**
     * Invalidate a set of keys in a cache engine - this method DELETES keys in the cache engine
     * @param {object} cacheClient - cache client instance
     * @param {object} keyList - array of keys to invalidate
     * @param callback
     */
    invalidateKeys: function (cacheClient, keyList, callback) {
        // TODO: validate inputs

        cacheClient.del(keyList, function handleDel(err) {
            if (err) {
                return callback(err);
            } else {
                return callback(null);
            }
        });
    },

    /**
     * Invalidate a set of keys in a cache engine based on a key pattern - this method DELETES keys in the cache engine
     * @param {object} cacheClient - cache client instance
     * @param {string} pattern - key pattern to search for keys to invalidate
     * @param callback
     */
    invalidateAllKeysByPattern: function (cacheClient, pattern, callback) {
        // TODO
    }
};
