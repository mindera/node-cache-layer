'use strict';

var util = require('./lib/util');
var manager = require('./lib/manager');
var scheduler = require('./lib/scheduler');
var httpClient = require('./lib/http');


module.exports = {
    /**
     * Initializes the module
     * @param {object} options - initialization options
     * @returns {exports} - cacheLayer instance
     */
    init: function(options) {
        options = util.sanitizeOptions(options);

        this.cacheClient = util.requireCacheClient(options.cache.client).init(options.cache);
        this.httpClient = httpClient.init(options.http);
        this.manager = manager.init(options.manager);
        this.scheduler = scheduler.init(options.scheduler);

        if (options.scheduler.requireLeadership) {
            this.coordinatorClient = util.requireCoordinatorClient(options.coordinator.client).init(options.coordinator);
            this.coordinatorClient.startElection();
        }

        return this;
    },

    /**
     * Refresh keys on demand based on a list of cache keys and correspondent http requests
     * @param {object} keyList - map of cache keys and http requests
     * @param callback
     */
    refreshKeys: function(keyList, callback) {
        var managerFunc = this.manager.refreshKeys;

        // TODO: validate inputs

        managerFunc(this.httpClient, this.cacheClient, keyList, function handleKeyRefresh(err) {
            if (err) {
                return callback(err);
            }

            return callback(null);
        });
    },

    /**
     * Refresh keys on demand based on a list of cache keys and correspondent values
     * @param {object} keyList - map of cache keys and values
     * @param callback
     */
    refreshKeysWithValue: function (keyList, callback) {
        var managerFunc = this.manager.refreshKeysWithValue;

        // TODO: validate inputs

        managerFunc(this.cacheClient, keyList, function handleKeyRefresh(err) {
            if (err) {
                return callback(err);
            }

            return callback(null);
        });
    },

    /**
     * Refresh a single key on demand based on a correspondent http request and return the response
     * @param {object} key - map of a single key and http request
     * @param callback
     */
    refreshAndReturnKey: function (key, callback) {
        var managerFunc = this.manager.refreshAndReturnKey;

        // TODO: validate inputs

        managerFunc(this.httpClient, this.cacheClient, key, function handleKeyRefreshAndReturn(err, response) {
            if (err) {
                return callback(err);
            }

            return callback(null, response);
        });
    },

    /**
     * Refresh keys on a background scheduler based on a list of cache keys and correspondent http requests
     * @param {array} args - array of argumentos to be passsed to the mapFunc
     * @param {function} mapFunc - reference to a mapping function which must generate a keyList to be refreshed
     * @param {object} scope - scope to apply to the mapFunc
     */
    refreshScheduler: function(args, mapFunc, scope){
        var schedulerFunc = this.scheduler.refreshKeys;

        // TODO: validate inputs

        var self = this;
        var handleKeyList = function (keyList) {
            schedulerFunc.apply(self.scheduler, [self.httpClient, self.cacheClient, self.coordinatorClient, keyList]);
        };

        args.push(handleKeyList);

        mapFunc.apply(scope || null, args);
    }
};
