'use strict';

var etcdClient = require('../coordination/etcd');

// test helper variables
var etcdOptions = {
    etcd: {
        host: 'localhost',
        port: 4001,
        leaderKey: '/master',
        keyExpireWindow: 5
    }
};

module.exports.validateInputs = {
    testInvalidOptions: function (test) {
        test.expect(4);

        test.throws(function () {
            etcdClient.init({
                etcd: {
                    host: 1,
                    port: 4001,
                    leaderKey: '/master',
                    keyExpireWindow: 5
                }
            });
        }, TypeError, 'it should throw a TypeError if etcd.host is not a string');

        test.throws(function () {
            etcdClient.init({
                etcd: {
                    host: 'localhost',
                    port: 'NaN',
                    leaderKey: '/master',
                    keyExpireWindow: 5
                }
            });
        }, TypeError, 'it should throw a TypeError if etcd.port is not a number');

        test.throws(function () {
            etcdClient.init({
                etcd: {
                    host: 'localhost',
                    port: 4001,
                    leaderKey: 1,
                    keyExpireWindow: 5
                }
            });
        }, TypeError, 'it should throw a TypeError if etcd.leaderKey is not a string');

        test.throws(function () {
            etcdClient.init({
                etcd: {
                    host: 'localhost',
                    port: 4001,
                    leaderKey: '/master',
                    keyExpireWindow: 'NaN'
                }
            });
        }, TypeError, 'it should throw a TypeError if etcd.keyExpireWindow is not a number');

        test.done();
    }
};

module.exports.validateFunctions = {
    testStartElection: function (test) {
        test.expect(1);

        var etcdElection = etcdClient.init(etcdOptions);

        etcdElection.startElection();

        test.ok(etcdElection.isRunning(), 'etcd election isRunning method should be OK');

        test.done();
    }
};
