'use strict';

var redis = require('../cache/redis');

// test helper variables
var redisClient;
var redisOptions = {
    client: 'fakeredis',
    redis: {
        host: 'localhost',
        port: 8001,
        enableOfflineQueue: true,
        retryMaxDelay: 10000
    }
};

module.exports.validateInputs = {
    testInvalidOptions: function (test) {
        test.expect(4);

        test.throws(function () {
            redis.init({
                client: 'fakeredis',
                redis: {
                    host: 1,
                    port: 4001,
                    enableOfflineQueue: true,
                    retryMaxDelay: 10000
                }
            });
        }, TypeError, 'it should throw a TypeError if redis.host is not a string');

        test.throws(function () {
            redis.init({
                client: 'fakeredis',
                redis: {
                    host: 'localhost',
                    port: 'NaN',
                    enableOfflineQueue: true,
                    retryMaxDelay: 10000
                }
            });
        }, TypeError, 'it should throw a TypeError if redis.port is not a number');

        test.throws(function () {
            redis.init({
                client: 'fakeredis',
                redis: {
                    host: 'localhost',
                    port: 4001,
                    enableOfflineQueue: 'NotABoolean',
                    retryMaxDelay: 10000
                }
            });
        }, TypeError, 'it should throw a TypeError if redis.enableOfflineQueue is not a boolean');

        test.throws(function () {
            redis.init({
                client: 'fakeredis',
                redis: {
                    host: 'localhost',
                    port: 4001,
                    enableOfflineQueue: true,
                    retryMaxDelay: 'NaN'
                }
            });
        }, TypeError, 'it should throw a TypeError if redis.retryMaxDelay is not a number');

        test.done();
    }
};

module.exports.validateFunctions = {
    setUp: function (callback) {
        redisClient = redis.init(redisOptions);
        callback();
    },

    testSetAndGetOperation: function (test) {
        test.expect(2);

        redisClient.set('foo', 'bar', function (err) {
            test.ifError(err);

            redisClient.get('foo', function (err, value) {
                test.equal(value, 'bar', 'the redis key "foo" should be "bar"');

                test.done();
            });
        });
    },

    testDelOperation: function (test) {
        test.expect(3);

        redisClient.set('zoo', 'bar', function (err) {
            test.ifError(err);

            redisClient.del(['zoo'], function (err, reply) {
                test.ifError(err);

                test.equal(reply, 1, 'the redis key "zoo" should be deleted');

                test.done();
            });
        });
    }

};
