'use strict';

var http = require('http');
var httpClient = require('../lib/http');

// test helper variables
var httpServer = http.createServer(function (req, res) {
    res.writeHead(200);

    if (req.method === 'POST') {
        var resData = '';
        req.on('data', function(data) {
            resData += data;
        });

        req.on('end', function() {
            res.end(resData);
        });
    }

    if (req.method === 'GET') {
        if (req.url === '/json') {
            var resObj = {
                status: 'OK'
            };
            res.end(JSON.stringify(resObj));
        } else {
            res.end('OK');
        }
    }
});

var httpOptions = {
    hostname: 'localhost',
    port: 8001,
    method: 'GET',
    path: '/'
};

module.exports.validateInputs = {
    testInvalidTimeouts: function (test) {
        test.expect(2);

        test.throws(function () {
            httpClient.init({requestTimeout: 'NaN', responseTimeout: 1000});
        }, TypeError, 'it should throw a TypeError if requestTimeout is not a number');

        test.throws(function () {
            httpClient.init({requestTimeout: 1000, responseTimeout: 'NaN'});
        }, TypeError, 'it should throw a TypeError if responseTimeout is not a number');

        test.done();
    }
};

module.exports.validateFunctions = {
    setUp: function (callback) {
        httpServer.listen(8001, 'localhost', function () {
            return callback();
        });
    },

    testGetRequestWithPlainTextResponse: function (test) {
        test.expect(1);

        var client = httpClient.init({requestTimeout: 1000, responseTimeout: 1000});

        httpOptions.path = '/plain';
        client.request(httpOptions, function handleRequest(err, res) {
            test.equal(res, 'OK', 'http response should have been OK');

            test.done();
        });
    },

    testGetRequestWithJSONResponse: function (test) {
        test.expect(1);

        var client = httpClient.init({requestTimeout: 1000, responseTimeout: 1000});

        httpOptions.path = '/json';
        client.request(httpOptions, function handleRequest(err, res) {
            test.equal(res.status, 'OK', 'http response should have been { status: "OK" }');

            test.done();
        });
    },

    testPostRequest: function (test) {
        test.expect(1);

        var client = httpClient.init({requestTimeout: 1000, responseTimeout: 1000});

        httpOptions.method = 'POST';
        httpOptions.path = '/';
        httpOptions.postData = 'OK';
        client.request(httpOptions, function handleRequest(err, res) {
            test.equal(res, 'OK', 'http response should have been OK');

            test.done();
        });


    }

};
