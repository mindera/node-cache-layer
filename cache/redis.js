'use strict';

var redis;

module.exports = {

    /**
     * Initializes the redis client module
     * @param {object} options - redis client options
     * @returns {exports} - redisClient instance
     */
    init: function (options) {

        if (options.redis.host) {
            if (typeof options.redis.host !== 'string') {
                throw new TypeError('redis.host is not a string: ' + typeof options.redis.host);
            }
        }

        if (options.redis.port) {
            if (typeof options.redis.port !== 'number') {
                throw new TypeError('redis.port is not a number: ' + typeof options.redis.port);
            }
        }

        if (options.redis.enableOfflineQueue) {
            if (typeof options.redis.enableOfflineQueue !== 'boolean') {
                throw new TypeError('redis.enableOfflineQueue is not a boolean: ' +
                    typeof options.redis.enableOfflineQueue);
            }
        }

        if (options.redis.retryMaxDelay) {
            if (typeof options.redis.retryMaxDelay !== 'number') {
                throw new TypeError('redis.retryMaxDelay is not a number: ' + typeof options.redis.retryMaxDelay);
            }
        }

        redis = require(options.client);

        var redisHost = options.redis.host || 'localhost';
        var redisPort = options.redis.port || 6379;
        var redisOptions = {
            enable_offline_queue: options.redis.enableOfflineQueue || true,
            retry_max_delay: options.redis.retryMaxDelay || 10000
        };

        this.redisClient = redis.createClient(redisPort, redisHost, redisOptions);

        // TODO: implement redisClient.auth

        this.redisClient.on('error', function handleClientError(err) {
            // TODO: handle error properly
        });

        this.redisClient.on('reconnecting', function handleClientReconnect() {
            // TODO: handle reconnect properly
        });

        return this;
    },

    /**
     * SET a key to a value
     * @param {string} key
     * @param {string} value
     * @param callback
     */
    set: function (key, value, callback) {
        this.redisClient.set(key, value, function handleSet(err) {
            if (err) {
                return callback(err);
            } else {
                return callback(null);
            }
        });
    },

    /**
     * GET a key value
     * @param {string} key
     * @param callback
     */
    get: function (key, callback) {
        this.redisClient.get(key, function (err, reply) {
            if (err) {
                return callback(err);
            } else {
                return callback(null, reply);
            }
        });
    },

    /**
     * DEL a set of keys
     * @param {Array} keys
     * @param callback
     */
    del: function (keys, callback) {
        this.redisClient.del(keys, function (err, reply) {
            if (err) {
                return callback(err);
            } else {
                return callback(null, reply);
            }
        });
    }
};
